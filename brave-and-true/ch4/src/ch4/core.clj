(ns ch4.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

(defn titleize
  [topic]
  (str topic " for the Brave and True"))

(map titleize ["Hamster" "Ragnarok"])

(map titleize '("Empathy" "Decorating"))

(map titleize #{"Elbows" "Soap Carving"})

(map #(titleize (first %)) {:uncomfortable-thing "Winking"})

(def human-consumption [8.1 7.3 6.6 6.0])

(def critter-consumption [0.0 0.2 0.3 1.1])

(defn unify-diet-data
  [human critter]
  {:human human
   :critter critter})

(map unify-diet-data human-consumption critter-consumption)

(def sum #(reduce + %))
(def avg #(/ (sum %) (count %)))
(defn stats
  [numbers]
  (map #(% numbers) [sum count avg]))

(def numbers [3 4 10])

(sum numbers)

(stats [3 4 10])
(sum [3 4 10])

(def identities
  [{:alias "Batman" :real "Bruce Wyne"}
   {:alias "Spider-Man" :real "Peter Parker"}
   {:alias "Santa" :real "Your mom"}
   {:alias "Easter Bunny" :real "Your dad"}])

(map :real identities)

(reduce (fn [new-map  [key val]]
          (assoc new-map key (inc val)))
        {}
        {:max 31, :min 11})

(reduce (fn [new-map [key val]]
          (if (> val 4)
            (assoc new-map key val)
            new-map))
        {}
        {:human 4.1 :critter 3.9})

(map inc [1 2 3 4])

(defn rmap
  [fun coll]
  (reduce (fn [new-coll v]
            (conj new-coll (fun v)))
   []
   coll))

(conj {:blu 2} {:bla 1})

(rmap inc [1 2 3 4])

(def food-journal
  [{:month 1 :day 1 :human 5.3 :critter 2.3}
   {:month 1 :day 2 :human 5.1 :critter 2.0}
   {:month 2 :day 2 :human 4.9 :critter 2.1}
   {:month 3 :day 1 :human 4.2 :critter 3.3}
   {:month 3 :day 2 :human 4.0 :critter 3.8}
   {:month 4 :day 1 :human 3.7 :critter 3.9}
   {:month 4 :day 2 :human 3.7 :critter 3.6}])

(take-while #(< (:month %) 3) food-journal)

(some #(and (= (:month %) 3) %) food-journal)

(cons "bla" "bla")

(cons)

(def vampire-database
  {0 {:makes-blood-puns? false, :has-pulse? true, :name "McFishwich"}
   1 {:makes-blood-puns? false, :has-pulse? true, :name "McMackson"}
   2 {:makes-blood-puns? true, :has-pulse? false, :name "Damon Salvatore"}
   3 {:makes-blood-puns? true, :has-pulse? true, :name "Mickey Mouse"}})

(defn vampire-related-details
  [social-security-number]
  (Thread/sleep 1000)
  (get vampire-database social-security-number))

(defn vampire?
  [record]
  (and (:makes-blood-puns? record)
       (not (:has-pulse? record))
       record))

(defn identify-vampire
  [social-security-numbers]
  (first (filter vampire?
                 (map vampire-related-details social-security-numbers))))

(def not-vampire? (complement vampire?))

(defn identify-humans
  [social-security-numbers]
  (first (filter #(not (vampire? %))
                 (map vampire-related-details social-security-numbers))))

(time (vampire-related-details 0))

(time (def mapped-details (map vampire-related-details (range 0 10000000))))

(time (first mapped-details))

(time (identify-vampire (range 0 100000)))

(concat (take 8 (repeat "na")) ["Batman!"])

(take 3 (repeatedly (fn [] (rand-int 10))))

(defn even-numbers
  ([] (even-numbers 0))
  ([n] (cons n (lazy-seq (even-numbers (+ n 2))))))

(take 10 (even-numbers))

(cons 0 '(1 2))

(cons 0 (seq 2))

(cons 0 (+ 0 2))

(defn varargs
  [& args]
  args)

(varargs [1 2 4])
