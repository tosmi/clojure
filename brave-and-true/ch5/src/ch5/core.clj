(ns ch5.core
  (:gen-class))

(defn sum
  ([vals] (sum vals 0))
  ([vals accumulating-total]
   (if (empty? vals)
     accumulating-total
     (sum (rest vals) (+ (first vals) accumulating-total)))))

(sum [1 2 3 4])

(def character
  {:name "Smooches McCutes"
   :attributes {:intelligence 10
                :strength 4
                :dexterity 5}})

(def c-int (comp :intelligence :attributes))
(def c-str (comp :strength :attributes))

(c-int character)
(c-str character)

(defn spell-slots
  [c]
  (int (inc (/ (c-int c) 2))))

(spell-slots character)

(def spell-slots-comp (comp int inc #(/ % 2) c-int))

(spell-slots-comp character)

(defn two-comp
  [f g]
  (fn [& args]
    (f (apply g args))))

((two-comp inc *) 2 )

(* 3)

(defn my-comp
  [first & rest]
  (if (empty? rest)
    (fn [& args]
      (apply first args))
   (my-comp rest)))



((my-comp inc *) 4)

(comp)
