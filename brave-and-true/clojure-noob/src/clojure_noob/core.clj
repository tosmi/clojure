(ns clojure-noob.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "I'm a teapot!"))

(println "Cleanliness is next to godliness")

(defn train
  []
  (println "Choo choo!"))

(+ 1 (* 2 3) 4
   )

(def severity :mild)

(defn error-message
  ([] (error-message :fuck))
  ([severity] (str "OH GOD! IT'S A DISASTER! WE'RE "
                   (if (= severity :mild)
                     "MILDLY ICONVENIENCED!"
                     "DOOOOMED!"))))

(error-message :mild)
(error-message :fuck)
(error-message)

(defn announce-treasure-location
  [{lat :lat lng :lng}]
  (println (str "Treasure lat: " lat))
  (println (str "Treasure lng: " lng)))

(defn announce-treasure-location
  [{:keys [lat lng] :as location}]
  (println (str "Treasure lat: " lat))
  (println (str "Treasure lng: " lng)))

(announce-treasure-location {:lat 22.2 :lng 23.1})
