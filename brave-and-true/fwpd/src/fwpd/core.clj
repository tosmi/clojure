(ns fwpd.core
  (:gen-class))

(use '[clojure.string :only (join)])

(def filename "suspects.csv")

(slurp filename)

(def vamp-keys [:name :glitter-index])

(defn str->int
  [str]
  (Integer. str))

(def conversions {:name identity
                  :glitter-index str->int})

(defn convert
  [vamp-key value]
  ((get conversions vamp-key) value))

(convert :glitter-index "3")
(convert :name "toni")

(defn parse
  [string]
  (map #(clojure.string/split % #",")
       (clojure.string/split string #"\n")))

(parse (slurp filename))

(defn mapify
  [rows]
  (map (fn [unmapped-row]
         (reduce (fn [row-map [vamp-key value]]
                   (assoc row-map vamp-key (convert vamp-key value)))
                 {}
                 (map vector vamp-keys unmapped-row)))
       rows))

(map vector vamp-keys ["Bella Swan" "0"])

(vector :name "Bella Swan")

(first (mapify (parse (slurp filename))))

(defn glitter-filter
  [minimum-glitter records]
  (filter #(>= (:glitter-index %) minimum-glitter) records))

(glitter-filter 3 (mapify (parse (slurp filename))))

(def suspects (mapify (parse (slurp filename))))

(defn to-list
  [list]
  (reduce (fn [a map]
            (cons (:name map) a))
          []
          list))

(def suspects (to-list (glitter-filter 3 (mapify (parse (slurp filename))))))

(defn append-suspect
  [s suspects]
  (cons  s suspects))

(append-suspect {:name "Toni Schmidbauer" :glitter-index 3} suspects)

(defn has-name?
  [m]
  (contains? m :name))

(has-name? {:name "Toni Schmidbauer" :glitter-index 3})

(defn has-glitter-index?
  [m]
  (contains? m :glitter-index))

(def validations {:name has-name?
                  :glitter-index has-glitter-index?})

(defn validate
  [m record]
  (every? true? (map #((get m %) record) (keys m))))

(keys validations)

(get validations :name)

(validate validations {:name "Toni" :glitter-index 3 :bla 2})

(every? true? '(true true))

(reduce (fn [a [key value]] (cons value a)) [] {:a "a" :b "b"})

(cons "a" ["b" "c"])

(def suspects (mapify (parse (slurp filename))))

suspects

(defn to-csv
  [suspects]
  (reduce (fn [s suspect]
            (str s (join "," [(:name suspect) (str (:glitter-index suspect) "\n")])))
          ""
          suspects))

(str "bla "  "Toni")

(parse (to-csv suspects))
