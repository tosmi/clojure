;;;       1
;;;      2 3
;;;    4  5  6
;;;   7  8  9 10
;;; 11 12 13 14 15


(ns pegthing.core
  (:require [clojure.set :as set])
  (:gen-class))

(declare successful-move prompt-move game-over query-rows)

(defn tri*
  ([] (tri* 0 1))
  ([sum n]
   (let [new-sum (+ sum n)]
     (cons new-sum (lazy-seq (tri* new-sum (inc n)))))))

;; (tri*)

(def tri (tri*))

(defn triangular?
  [n]
  (= n (last (take-while #(>= n %) tri))))

(defn row-tri
  [n]
  (last (take n tri)))

(defn row-num
  [pos]
  (inc (count (take-while #(> pos %) tri))))

(defn connect
  [board max-pos pos neighbor destination]
  (if (<= destination max-pos)
    (reduce (fn [new-board [p1 p2]]
              (assoc-in new-board [p1 :connections p2] neighbor))
            board
            [[pos destination] [destination pos]])
    board))

(connect {} 15 1 2 4)

(defn connect-right
  [board max-pos pos]
  (let [neighbor (inc pos)
        destination (inc neighbor)]
    (if-not (or (triangular? neighbor) (triangular? pos))
      (connect board max-pos pos neighbor destination)
      board)))

(connect-right {} 15 8)

(defn connect-down-left
  [board max-pos pos]
  (let [row (row-num pos)
        neighbor (+ row pos)
        destination (+ 1 row neighbor)]
    (connect board max-pos pos neighbor destination)))

(connect-down-left {} 15 7)

(defn connect-down-right
  [board max-pos pos]
  (let [row (row-num pos)
        neighbor (+ 1 row pos)
        destination (+ 2 row neighbor)]
    (connect board max-pos pos neighbor destination)))

(defn add-pos
  [board max-pos pos]
  (let [pegged-board (assoc-in board [pos :pegged] true)]
    (reduce (fn [new-board connection-creation-fn]
              (connection-creation-fn new-board max-pos pos))
            pegged-board
            [connect-right connect-down-left connect-down-right])))

(add-pos {} 15 1)

(defn new-board
  [rows]
  (let [initial-board {:rows rows}
        max-pos       (row-tri rows)]
    (reduce (fn [board pos] (add-pos board max-pos pos))
            initial-board
            (range 1 (inc max-pos)))))

;;; {7 {:connections {2 4, 9 8}, :pegged true},
;;  1 {:pegged true, :connections {4 2, 6 3}},
;;  4 {:connections {1 2, 6 5, 11 7, 13 8}, :pegged true},
;;  15 {:connections {6 10, 13 14}, :pegged true},
;;  13 {:connections {4 8, 6 9, 11 12, 15 14}, :pegged true},
;;  :rows 5,
;;  6 {:connections {1 3, 4 5, 13 9, 15 10}, :pegged true},
;;  3 {:pegged true, :connections {8 5, 10 6}},
;;  12 {:connections {5 8, 14 13}, :pegged true},
;;  2 {:pegged true, :connections {7 4, 9 5}},
;; 11 {:connections {4 7, 13 12}, :pegged true},
;;  9 {:connections {2 5, 7 8}, :pegged true},
;;  5 {:pegged true, :connections {12 8, 14 9}},
;;  14 {:connections {5 9, 12 13}, :pegged true},
;;  10 {:connections {3 6, 8 9}, :pegged true},
;;  8 {:connections {3 5, 10 9}, :pegged true}}

(new-board 5)

(defn pegged?
  [board pos]
  (get-in board [pos :pegged]))

(defn remove-peg
  [board pos]
  (assoc-in board [pos :pegged] false))

(defn place-peg
  [board pos]
  (assoc-in board [pos :pegged] true))

(defn move-peg
  [board p1 p2]
  (place-peg (remove-peg board p1) p2))

(defn valid-moves
  [board pos]
  (into {}
        (filter (fn [[destination jumped]]
                  (and (not (pegged? board destination))
                       (pegged? board jumped)))
                (get-in board [pos :connections]))))

(valid-moves (remove-peg (new-board 15) 4) 1)

(defn valid-move?
  [board p1 p2]
  (get (valid-moves board p1) p2))

(defn make-move
  [board p1 p2]
  (if-let [jumped (valid-move? p1 p2)]
    (move-peg (remove-peg board jumped) p1 p2)))

(defn can-move?
  [board]
  (some (comp not-empty (partial valid-moves board))
        (map first (filter #(get (second %) :pegged) board))))

(map first (filter #(get (second %) :pegged) (new-board 3)))

(new-board 3)

(def alpha-start 97)
(def alpha-end 123)
(def letters (map (comp str char) (range alpha-start alpha-end)))
(def pos-chars 3)

(def ansi-styles
  {:red   "[31m"
   :green "[32m"
   :blue  "[34m"
   :reset "[0m"})

(defn ansi
  "Produce a string which will apply an ansi style"
  [style]
  (str \u001b (style ansi-styles)))

(defn colorize
  "Apply ansi color to text"
  [text color]
(str (ansi color) text (ansi :reset)))

(str (char 97))

(defn render-pos
  [board pos]
  (str (nth letters (dec pos))
      (if (get-in board [pos :pegged])
        (colorize "0" :blue)
        (colorize "-" :read))))

(defn row-positions
  [row-num]
  (range (inc (or (row-tri (dec row-num)) 0))
         (inc (row-tri row-num))))

(defn row-padding
  [row-num rows]
  (let [pad-length (/ (* (- rows row-num) pos-chars) 2)]
    (apply str (take pad-length (repeat " ")))))

(defn render-row
  [board row-num]
  (str (row-padding row-num (:rows board))
       (clojure.string/join " " (map (partial render-pos board)
                                     (row-positions row-num)))))
