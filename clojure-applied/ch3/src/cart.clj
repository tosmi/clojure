(ns cart
  (:require [money :refer [make-money +$ *$]]))

(defrecord CatalogItem [number dept desc price])
(defrecord Cart [number customer line-items settled?])
(defrecord LineItem [quantity catalog-item price])
(defrecord Customer [cname email membership-number])

(def customer (->Customer "Toni Schmidbauer" "toni@stderr.at" 1243))

(def shirt (->CatalogItem 664 :clothing "polo shirt L" (make-money 2515)))
(def pants (->CatalogItem 621 :clothing "khaki pants" (make-money 3500)))

(def cart
  (->Cart 116
          customer
          [
           (->LineItem 3 shirt (make-money 7545))
           (->LineItem 1 pants (make-money 3500))
           ]
          true))

(defn -main []
  (println "hello")
  (m/make-money 100))
