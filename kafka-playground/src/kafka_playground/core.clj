(ns kafka-playground.core
  (:require [kinsky.client :as client]))

(defn produce
  []
  (let [p (client/producer {:bootstrap.servers "localhost:9092"}
                           (client/keyword-serializer)
                           (client/edn-serializer))]
    (client/send! p "test" :test "test")))

(defn consume []
  (let [c (client/consumer {:bootstrap.servers "localhost:9092"
                            :group.id          "mygroup"}
                           (client/keyword-deserializer)
                           (client/edn-deserializer))]
    (client/subscribe! c "test")
    (print (client/poll! c 100))))

(defn -main [args]
  (case args
    "p" (produce)
    "c" (consume)))
