(ns programming-clojure.ch3
  (:require [clojure.string :refer [join]]))

(seq [1 2 3])

(seq? (cons 0 [1 2 3]))

(first {:name "toni" :age 16})
(rest {:name "toni" :age 16})

(rest (set [0 1 2 3 4 4]))

#{:the :quick :brown :fox}

(sorted-set :the :quick :brown :fox)

{:a 1 :b 2 :c 3}

(cons 0 [1 2 3])
(conj [1 2 3] 0)
(conj '(1 2 3) 0)

(rest [1 2 3])
(list? (rest [1 2 3]))
(seq? (rest [1 2 3]))

(range 10 20 2)
(repeat 5 "x")

(take 10 (iterate inc 1))

(def whole-numbers (iterate inc 1))

(take 20 (repeat 1))
(take 10 (cycle (range 3)))

(interleave whole-numbers ["A" "B" "C"] ["a" "b" "c"])
(interleave whole-numbers ["A" "B" "C"] )

(interpose "," ["apple" "bananas" "grapes"])
(apply str (interpose "," ["apple" "bananas" "grapes"]))

(join \, ["a" "b" "c"])

(vector 1 2 3 4)

(filter #(< 2 %) (range 10))

(take 10 (filter even? whole-numbers))

(def vowel? #{\a\e\i\o\u})

(def consonant? (complement vowel?))

(sort-by #(.toString %) [42 1 7 11])

(sort-by :grade < [{:grade 83} {:grade 90 } {:grade 77}])

(for [word ["the" "quick" "brown" "fox"]]
  (format "<p>%s</p" word))

(take 10 (for [n whole-numbers :when (even? n)] n))

(for [n whole-numbers :while (even? n)] n)

(for [file "ABCDEFGHF"
      rank (range 1 9)]
  (format "%c%d" file rank))

(ns examples.primes)

(def primes
  (concat [2 3 5 7]
          (lazy-seq
           (let [primes-from
                 (fn primes-from [n [f & r]]
                   (if (some #(zero? (rem n %))
                             (take-while #(<= (* % %) n) primes))
                     (recur (+ n f) r)
                     (lazy-seq (cons n (primes-from (+ n f) r)))))
                 wheel (cycle [2 4 2 4 6 2 6 5 2 5 6 6 2 6  5  2
                               6 4 6 8 4 2 4 2 4 8 6 4 6 2  4  6
                               2 6 6 4 2 4 6 2 6 4 2 4 2 10 2 10])]
             (primes-from 11 wheel)))))

(take 10 primes)

(rem 11 3)

(take-while #(<= (* % %) 11) [2 3 5 7])

(require '[examples.primes :refer :all])
(def ordinals-and-primes (map vector (iterate inc 1) primes))

(take 5 (drop 1000 ordinals-and-primes))

(take 100 (cycle [1 2 3]))

(def x (for [i (range 1 13)] (do (println i) i)))

(doall x)

(System/getProperties)
(rest (System/getProperties))

(let [m (re-matcher #"\w+" "the big brown fox")]
  (re-find m))

(import 'java.io.File)
(defn minutes-to-millis [mins] (* mins 1000 60))

(defn recently-modified? [file]
  (> (.lastModified file)
     (- (System/currentTimeMillis) (minutes-to-millis 30))))

(filter recently-modified? (file-seq (File. ".")))

(file-seq  (File "."))

(.getName (first (seq  (.listFiles (File. ".")))))

(use 'clojure.java.javadoc)

(javadoc Strin
         g)
