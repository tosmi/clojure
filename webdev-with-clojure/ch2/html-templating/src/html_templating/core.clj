(ns html-templating.core
  (:require [selmer.parser :as selmer]
            [selmer.filters :as filters]
            [selmer.middleware :refer [wrap-error-page]]))

;; selmer caches templates by default
(selmer/cache-off!)

(selmer/render "Hello {{name}}" {:name "World"})

(selmer/render-file "hello.html" {:name "World" :items (range 10)})
(selmer/render-file "base.html" {})

(selmer/render "Hello {{user.first|upper}} {{user.last}}" {:user {:first "John" :last "Doe"}} )

(filters/add-filter! :empty? empty?)

(selmer/render "{% if files|empty? %}no files{% else %}files{% endif %}" {:files []})

(selmer/render "{{x}}" {:x "<script>owned()</script>"})
(filters/add-filter! :foo
                     (fn [x] [:safe (.toUpperCase x)]))
(selmer/render "{{x|foo}}" {:x "<div>I'm safe</div>"})

(selmer/add-tag!
 :image
 (fn [args context-map]
   (str "<img src=" (first args) "/>")))

(selmer/render "{% image \"http://foo.com/logo.png\" %}" {})

(selmer/add-tag!
 :uppercase
 (fn [args context-map block]
   (.toUpperCase (get-in block [:uppercase :content])))
 :enduppercase)

(selmer/render "{% uppercase %}foo {{bar}} baz{% enduppercase %}" {:bar "injected"})

(defn render []
  (wrap-error-page
   (fn [template]
     {:status 200
      :body (selmer/render-file template {})})))

((render) "error.html")



(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))
