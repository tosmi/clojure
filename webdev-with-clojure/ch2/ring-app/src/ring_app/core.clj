(ns ring-app.core
  (:require [ring.adapter.jetty :as jetty]
            [ring.util.http-response :as response]
            [ring.middleware.reload :refer [wrap-reload]]
            [muuntaja.middleware :as muuntaja]
            [reitit.ring :as reitit]))

(defn wrap-nocache [handler]
  (fn [request]
    (-> request
        handler
        (assoc-in [:headers "Pragma"] "no-cache"))))

(defn response-handler [req]
  (response/ok
   (str "<html><body>you IP is: "
        (:remote-addr req)
        "</body></html")))

(defn json-handler [req]
  (response/ok
   {:result (get-in req [:body-params :id])})
  )

(def routes
  [
   ["/" {:get response-handler
         :post response-handler}]
   ["/echo/:id" (fn [{{:keys [id] :as path} :path-params :as rest}]
                  (response/ok (str "<p>The value is: " id "/" path "/" rest "</p>")))]
   ["/api"
    {:middleware [wrap-formats]}
    ["/multiply"
     {:post
      (fn [{{:keys [a b]} :body-params}]
        (response/ok {:result (* a b)}))}]]])

(def handler
  (reitit/routes
   (reitit/ring-handler
    (reitit/router routes))
   (reitit/create-resource-handler
    {:path "/"})
   (reitit/create-default-handler
    {:not-found
     (constantly (response/not-found "404 - Page not found"))
     :method-not-allowed
     (constantly (response/method-not-allowed "405 - Not allowed"))
     :not-acceptable
     (constantly (response/not-acceptable "406 - Not acceptable"))
     })))

(defn wrap-formats [handler]
  (-> handler
      (muuntaja/wrap-format)))

(defn -main []
  (jetty/run-jetty
   (-> handler
       var
       wrap-nocache
       wrap-reload)
   {:port 3000
    :join? false}))
